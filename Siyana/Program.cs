﻿using Siyana.Controllers;
using System;
using System.Threading;

namespace Siyana
{
    class Program
    {
        static void Main(string[] args)
        {
            CommController CommCon = new CommController();
            Console.Out.WriteLine(CommCon.Greeting());
            Thread.Sleep(2000);
            Console.Out.WriteLine("Would you like to read the news?");
            Console.Out.WriteLine(CommCon.ResearchNews());
            Console.ReadKey();            
        }
    }
}
