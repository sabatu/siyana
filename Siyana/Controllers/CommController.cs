﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using System.Linq;
using Siyana.Model;
using System.Xml;
using System.ServiceModel;

namespace Siyana.Controllers
{
    public class CommController
    {
        public CommController()
        {

        }

        public string Greeting()
        {
            return "Hello, Sabatu.";
        }

        public string ResearchNews()
        {
            WebRequest getNews = WebRequest.Create("https://news.google.com/rss");
            WebResponse newsResponse = getNews.GetResponse();
            Stream newsStream = newsResponse.GetResponseStream();

            using (var reader = new StreamReader(newsResponse.GetResponseStream()))
            {
                string result = reader.ReadToEnd();

                XElement googleNewsHeadlines = XElement.Load("https://news.google.com/rss"); 

                //Read parent node.
                XNode parentXMLNode = googleNewsHeadlines.FirstNode;
                XElement parentXMLElement = parentXMLNode as XElement;

                //Iterate into news headline items.
                XNode newsItemsXML = parentXMLElement.FirstNode;
                XElement newsXML = newsItemsXML as XElement;      
                List<XNode> listItems = newsXML.NodesAfterSelf().ToList();

                //Create data structure to retrieve headline titles.
                List<string> testTitles = new List<string>();
                string newsStreamString = String.Empty;
                int newsStories = 1;

                //Loop through each node - if the node is an item, that means it is a news story.. so grab the headline.
                foreach (XNode employee in listItems)
                {
                    XElement nextNode = employee as XElement;
                    if (nextNode.Name == "item")
                    {
                        testTitles.Add(Convert.ToString(nextNode.Element("title").Value));
                        newsStreamString +=  "\n" + Convert.ToString(newsStories++)  + ".) " + Convert.ToString(nextNode.Element("title").Value) + "\n";
                    }
                    
                }
                

                return newsStreamString;
            }

            
        }
    }
}
